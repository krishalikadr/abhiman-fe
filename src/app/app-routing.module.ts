import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.default)
  },
  {
    path: 'commons',
    loadChildren: () => import('./common/common.module').then(m => m.default)
  },
  {
    path: 'sidebar-items',
    loadChildren: () => import('./sidebar-items/sidebar-items.module').then(m => m.default)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
