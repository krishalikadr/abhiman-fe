import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import SharedModule from "../shared.module";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { NavbarComponent } from "./navbar/navbar.component";

const routes: Routes = [
    {
        path: 'navbar',
        component: NavbarComponent
    },
    {
        path: 'sidebar',
        component: SidebarComponent
    }
]

@NgModule({
    imports: [SharedModule, RouterModule.forChild(routes)],
    declarations: [NavbarComponent, SidebarComponent],
    exports: [RouterModule]
})
export default class CommonModule { }