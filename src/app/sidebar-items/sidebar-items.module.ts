import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import SharedModule from "../shared.module";
import { CustomersComponent } from "./customers/customers.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { EmployeesComponent } from "./employees/employees.component";
import { FinancialAccComponent } from "./financial-acc/financial-acc.component";
import { FormEditsComponent } from "./form-edits/form-edits.component";
import { FormsComponent } from "./forms/forms.component";
const routes: Routes = [

    {
        path: 'customers',
        component: CustomersComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'employees',
        component: EmployeesComponent
    },
    {
        path: 'financial-acc',
        component: FinancialAccComponent
    },
    {
        path: 'form-edits',
        component: FormEditsComponent
    },
    {
        path: 'forms',
        component: FormsComponent
    }
]

@NgModule({
    imports: [SharedModule, RouterModule.forChild(routes)],
    declarations: [CustomersComponent, DashboardComponent, EmployeesComponent, FinancialAccComponent, FormEditsComponent, FormsComponent],
    exports: [RouterModule]
})
export default class CommonModule { }