import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialAccComponent } from './financial-acc.component';

describe('FinancialAccComponent', () => {
  let component: FinancialAccComponent;
  let fixture: ComponentFixture<FinancialAccComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialAccComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialAccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
