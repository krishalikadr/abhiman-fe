import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEditsComponent } from './form-edits.component';

describe('FormEditsComponent', () => {
  let component: FormEditsComponent;
  let fixture: ComponentFixture<FormEditsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormEditsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEditsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
