import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import SharedModule from "../shared.module";
import { LoginComponent } from "./login/login.component";
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    }
]

@NgModule({
    imports: [SharedModule, RouterModule.forChild(routes), FormsModule],
    declarations: [LoginComponent],
    exports: [RouterModule]
})
export default class AuthModule { }