import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public checkboxValue: boolean;
  constructor(private authService: AuthService) { this.checkboxValue = true; }

  ngOnInit(): void {
  }


  public onSaveCredentialsChanged(value: boolean) {
    this.checkboxValue = value;
  }

  onSignin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    console.log(email);

    if (email === 'sampleuser@gmail.com') {
      this.authService.signinUser(email, password);

    }
  }

}
